import { Team } from './team';

type Teams = {
  home: Team,
  visit: Team,
}

type Score = {
  home: number,
  visit: number,
}

interface IMatch {
  getTeams(): Teams;
  getScoreboard(): Score;
  calculateMatchScore();
  calculateMatchWinner(): 'home' | 'visit' | 'draw';
  play(): 'home' | 'visit' | 'draw';
}

export class Match implements IMatch {
  private scoreboard: Score = { home: 0, visit: 0 };

  constructor(private readonly teams: Teams) {}

  getTeams(): Teams {
    return this.teams;
  }

  getScoreboard(): Score {
    return this.scoreboard;
  }

  registerScore(team: 'home' | 'visit'){
    const randomPlayerIndex = Math.floor(Math.random() * 5);
    this.teams[team].getPlayers()[randomPlayerIndex].addGoal();
    this.scoreboard[team] += 1;
  }

  /**
   * Cálculo da pontuação dos times
   * 
   * Cada time tem uma probablildade inicial, sendo o time da casa de 12% e o visitante de 10%
   * Considerando as habilidades dos jogadores, cada time pode ter sua chance de gol aumentada em 40%
   * Essa chance é testada 10 vezes, ou seja, um time perfeito, com muita sorte, pode fazer no máximo 10 gols em uma partida
   */
  calculateMatchScore(): void {
    if (this.teams.home.getPlayers.length !== 5 || this.teams.visit.getPlayers.length !== 5) {
      throw new Error('Invalid player amount.');
    }

    //determinação da habilidade geral
    let homeChance = 0.12;
    this.teams.home.getPlayers().forEach((player) => {
      homeChance += player.getSkill() * 0.0008;
    });
    let visitChance = 0.1;
    this.teams.visit.getPlayers().forEach((player) => {
      visitChance += player.getSkill() * 0.0008;
    });

    //gols
    for(let i = 0; i < 10; i++){
      if(Math.random() < homeChance){
        this.registerScore('home');
      }
      if(Math.random() < visitChance){
        this.registerScore('visit');
      }
    }
  }

  calculateMatchWinner(): 'home' | 'visit' | 'draw' {
    if(this.scoreboard.home === this.scoreboard.visit){
      this.teams.home.addPoints(1);
      this.teams.visit.addPoints(1);
      return 'draw';
    } else if(this.scoreboard.home > this.scoreboard.visit){
      this.teams.home.addPoints(3);
      return 'home';
    } else {
      this.teams.visit.addPoints(3);
      return 'visit';
    }
  }

  play(): 'home' | 'visit' | 'draw' {
    this.calculateMatchScore();
    return this.calculateMatchWinner();
  }
}
