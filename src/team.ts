import { Player, Position } from './player';

interface ITeam {
  addPlayer(player: Player): void;
  removePlayer(player: Player): boolean;
  getPoints(): number;
  validatePlayerPositions();
  addPoints(result: number): void;
  getPlayer(playerNumber: number): Player | null;
  getPlayers(): Player[];
}

export class Team implements ITeam {
  constructor(private team: Player[], private points: number = 0) {
    if (team.length > 5) {
      throw new Error('Team has too many players.');
    }
    const hasDuplicateShirtNumbers = team.some(
      (player, index) => team.some(
        (player2, index2) => player.getNumber() === player2.getNumber() && index !== index2,
      ),
    );
    if (hasDuplicateShirtNumbers) {
      throw new Error('Two players have the same shirt number.');
    }

    this.validatePlayerPositions();
  }

  validatePlayerPositions(){
    const goleiroCount = this.team.filter((player) => player.getPosition() === Position.Goleiro).length;
    const atacanteCount = this.team.filter((player) => player.getPosition() === Position.Atacante).length;
    const defensorCount = this.team.filter((player) => player.getPosition() === Position.Defensor).length;

    if(goleiroCount > 1){
      throw new Error('There are too many goalkeepers in the team.');
    } else if (atacanteCount > 2){
      throw new Error('There are too many attackers in the team.');
    } else if (defensorCount > 2){
      throw new Error('There are too many defenders in the team.');
    }
  }

  addPlayer(player: Player): void {
    this.validatePlayerPositions();
    this.team.push(player);
  }

  removePlayer(player: Player): boolean {
    const index = this.team.findIndex((item) => item.getNumber() === player.getNumber());

    if (index !== -1) {
      this.team.splice(index, 1);
      return true;
    }

    return false;
  }

  getPoints(): number {
    return this.points;
  }

  addPoints(result: number): void {
    this.points += result;
  }

  getPlayer(playerNumber: number): Player | null {
    const index = this.team.findIndex((item) => item.getNumber() === playerNumber);

    return this.team[index] || null;
  }

  getPlayers(): Player[] {
    return this.team;
  }
}
