export enum Position {
  Goleiro,
  Atacante,
  Defensor,
}

interface IPlayer {
  getSkill(): number;
  addGoal(): void;
  getGoals(): number;
  getNumber(): number;
  getPosition(): Position;
  getAge(): number;
}

export class Player implements IPlayer {
  constructor(
    private skill: number,
    private name: string,
    private readonly number: number,
    private readonly position: Position,
    private age: number,
    private goals: number = 0,
  ) {}

  getSkill(): number {
    return this.skill;
  }

  getName(): string {
    return this.name;
  }

  addGoal() {
    this.goals += 1;
  }

  getGoals() {
    return this.goals;
  }

  getNumber() {
    return this.number;
  }

  getPosition() {
    return this.position;
  }

  getAge() {
    return this.age;
  }
}
