function __validatePositive(value: number){
  if(value < 0){
    throw new Error('Invalid input value.');
  }
};

function __validateZeroTenRange(value: number){
  if(value < 0 || value > 10){
    throw new Error('Input value must be between 0 and 10.');
  }
};

export const calculate = {
  

  goleiro: (height: number, reflexes: number) => {
    let normalizedHeight = height;
    __validatePositive(height);
    __validateZeroTenRange(reflexes);
    if(height < 0){
      throw new Error('Invalid height');
    } else if (height > 210) {
      normalizedHeight = 210;
    }
    normalizedHeight = (normalizedHeight / 210) * 10;    
    return Math.floor(normalizedHeight) * 4 + reflexes * 6;
  },
  defensor: (covering: number, disarming: number) => {
    __validateZeroTenRange(covering);
    __validateZeroTenRange(disarming);
    return covering * 6 + disarming * 4;
  },
  atacante: (speed: number, technique: number) => {
    __validateZeroTenRange(speed);
    __validateZeroTenRange(technique);
    return speed * 4 + technique * 6;
  }
};

//bota verificações de > 10 e < 0