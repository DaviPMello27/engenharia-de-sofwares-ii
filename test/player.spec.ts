import { Player, Position } from '../src/player';

const goleiroData = {
  skill: 10,
  name: 'nome',
  number: 12,
  position: Position.Goleiro,
  age: 18,
}

describe('player.ts', () => {
  beforeEach(() => {});

  describe('Player', () => {
    it('/ Properties are assigned correctly.', () => {
      const goleiro = new Player(
        goleiroData.skill,
        goleiroData.name,
        goleiroData.number,
        goleiroData.position,
        goleiroData.age,
      );
      
      expect(goleiro.getSkill()).toEqual(goleiroData.skill);
      expect(goleiro.getName()).toEqual(goleiroData.name);
      expect(goleiro.getNumber()).toEqual(goleiroData.number);
      expect(goleiro.getPosition()).toEqual(goleiroData.position);
      expect(goleiro.getAge()).toEqual(goleiroData.age);
    });

    it('/ .addGoal() increases goal count.', () => {
      const atacante = new Player(0, '', 0, Position.Atacante, 18, 6);

      atacante.addGoal();
      
      expect(atacante.getGoals()).toEqual(7);
    });
    
  });
});
