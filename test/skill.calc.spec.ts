import { calculate } from '../src/skill.calc';

describe('calc.ts', () => {
  beforeEach(() => {});

  describe('#goleiro', () => {
    it('normalizes the height', () => {
      expect(() => {calculate.goleiro(-1, 0)}).toThrow(new Error('Invalid input value.'))

      expect(calculate.goleiro(220, 0)).toEqual(40);

      expect(calculate.goleiro(2300, 0)).toEqual(40);
      
      expect(calculate.goleiro(180, 2)).not.toEqual(40);
    });

    it('return the skill of the goleiro', () => {
      expect(calculate.goleiro(0, 0)).toEqual(0)

      expect(calculate.goleiro(0, 10)).toEqual(60)

      expect(calculate.goleiro(100, 0)).toEqual(16)

      expect(calculate.goleiro(210, 10)).toEqual(100)

      expect(() => {calculate.goleiro(210, -10)}).toThrow(new Error('Input value must be between 0 and 10.'));
      expect(() => {calculate.goleiro(210, 100)}).toThrow(new Error('Input value must be between 0 and 10.'));
    })
  });

  describe('#defensor', () => {
    it('return the skill of the defensor', () => {
      expect(calculate.defensor(0, 0)).toEqual(0)

      expect(calculate.defensor(0, 10)).toEqual(40)

      expect(calculate.defensor(10, 0)).toEqual(60)

      expect(calculate.defensor(10, 10)).toEqual(100)

      expect(() => {calculate.defensor(10, -10)}).toThrow(new Error('Input value must be between 0 and 10.'));
      expect(() => {calculate.defensor(-10, 10)}).toThrow(new Error('Input value must be between 0 and 10.'));
      
      expect(() => {calculate.defensor(10, 100)}).toThrow(new Error('Input value must be between 0 and 10.'));
      expect(() => {calculate.defensor(100, 10)}).toThrow(new Error('Input value must be between 0 and 10.'));
    })
  });

  describe('#atacante', () => {
    it('return the skill of the atacante', () => {
      expect(calculate.atacante(0, 0)).toEqual(0)

      expect(calculate.atacante(0, 10)).toEqual(60)

      expect(calculate.atacante(10, 0)).toEqual(40)

      expect(calculate.atacante(10, 10)).toEqual(100)

      expect(() => {calculate.atacante(10, -10)}).toThrow(new Error('Input value must be between 0 and 10.'));
      expect(() => {calculate.atacante(-10, 10)}).toThrow(new Error('Input value must be between 0 and 10.'));
      
      expect(() => {calculate.atacante(10, 100)}).toThrow(new Error('Input value must be between 0 and 10.'));
      expect(() => {calculate.atacante(100, 10)}).toThrow(new Error('Input value must be between 0 and 10.'));
    })
  });
});
