import { Player, Position } from '../src/player';
import { Team } from '../src/team';
import { Match } from '../src/match';

const badTeamPlayers = [
  new Player(0, '', 12, Position.Goleiro, 21),
  new Player(0, '', 13, Position.Atacante, 25),
  new Player(0, '', 14, Position.Atacante, 39),
  new Player(0, '', 15, Position.Defensor, 48),
  new Player(0, '', 16, Position.Defensor, 19),
];

const perfectTeamPlayers = [
  new Player(10, '', 12, Position.Goleiro, 21),
  new Player(10, '', 13, Position.Atacante, 25),
  new Player(10, '', 14, Position.Atacante, 39),
  new Player(10, '', 15, Position.Defensor, 48),
  new Player(10, '', 16, Position.Defensor, 19),
]

const badTeam = new Team(badTeamPlayers);
const perfectTeam = new Team(perfectTeamPlayers);

describe('match.ts', () => {
  beforeEach(() => {});

  describe('match', () => {
    it('/ Properties are assigned correctly.', () => {
      const match = new Match({
        home: perfectTeam,
        visit: badTeam,
      });

      expect(match.getTeams().home).toEqual(perfectTeam);
      expect(match.getTeams().visit).toEqual(badTeam);
    });

    it('/ .registerScore() increases scoreboard.', () => {
      const match = new Match({
        home: perfectTeam,
        visit: badTeam,
      });

      match.registerScore('home');
      match.registerScore('home');
      match.registerScore('visit');

      expect(match.getScoreboard().home).toEqual(2);
      expect(match.getScoreboard().visit).toEqual(1);
    });

    it('/ .registerScore() increases a player\' total goal count.', () => {
      const team = new Team([
        new Player(0, '', 0, Position.Goleiro, 0),
        new Player(0, '', 1, Position.Atacante, 0),
        new Player(0, '', 2, Position.Atacante, 0),
        new Player(0, '', 3, Position.Defensor, 0),
        new Player(0, '', 4, Position.Defensor, 0),
      ]);

      const match = new Match({
        home: team,
        visit: badTeam,
      });

      match.registerScore('home');

      expect(team.getPlayers().some((player) => player.getGoals() > 0)).toEqual(true);
    });

    it('/ .calculateMatchWinner() returns the winner.', () => {
      const match = new Match({
        home: perfectTeam,
        visit: badTeam,
      });

      match.registerScore('home');
      match.registerScore('home');
      match.registerScore('visit');

      expect(match.calculateMatchWinner()).toEqual('home');
    });

    it('/ .calculateMatchWinner() returns a draw.', () => {
      const match = new Match({
        home: perfectTeam,
        visit: badTeam,
      });

      match.registerScore('home');
      match.registerScore('visit');

      expect(match.calculateMatchWinner()).toEqual('draw');
    });

    it('/ .calculateMatchWinner() increases the winner team\'s points by 3.', () => {
      const team = new Team(perfectTeamPlayers);

      const match = new Match({
        home: team,
        visit: badTeam,
      });

      match.registerScore('home');
      match.calculateMatchWinner();
      expect(team.getPoints()).toEqual(3);
    });

    it('/ .calculateMatchWinner() increases the draw teams\' points by 1.', () => {
      const team = new Team(perfectTeamPlayers);
      const team2 = new Team(badTeamPlayers);

      const match = new Match({
        home: team,
        visit: team2,
      });

      match.registerScore('home');
      match.registerScore('visit');
      match.calculateMatchWinner();
      expect(team.getPoints()).toEqual(1);
      expect(team2.getPoints()).toEqual(1);
    });
  });
});
