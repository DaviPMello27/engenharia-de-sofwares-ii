import { Player, Position } from '../src/player';
import { calculate } from '../src/skill.calc';
import { Team } from '../src/team';

const teamPlayers = [
  new Player(10, '', 12, Position.Goleiro,  21),
  new Player(0,  '', 13, Position.Atacante, 25),
  new Player(0,  '', 14, Position.Atacante, 39),
  new Player(0,  '', 15, Position.Defensor, 48),
  new Player(0,  '', 16, Position.Defensor, 19),
];


describe('team.ts', () => {
  beforeEach(() => {});

  describe('team', () => {
    it('/ Properties are assigned correctly.', () => {
      const team = new Team(teamPlayers);

      expect(team.getPlayers()).toHaveLength(5);
      for(const i in teamPlayers){
        expect(team.getPlayers()[i]).toEqual(teamPlayers[i]);
      }
    });

    it('/ .addPlayer() inserts a player.', () => {
      const team = new Team([
        teamPlayers[0],
        teamPlayers[1],
        teamPlayers[2],
      ]);

      team.addPlayer(teamPlayers[3])

      expect(team.getPlayers()).toHaveLength(4);
    });

    it('/ .removePlayer() removes a player.', () => {
      const team = new Team([
        teamPlayers[0],
        teamPlayers[1],
        teamPlayers[2],
      ]);

      team.removePlayer(teamPlayers[2]);

      expect(team.getPlayers()).toHaveLength(2);
    });

    it('/ Assigning two players with the same shirt number throws an error.', () => {
      expect(() => {
        new Team([
          teamPlayers[0],
          teamPlayers[0],
          teamPlayers[2],
        ]);
      }).toThrow(new Error('Two players have the same shirt number.'));
    });

    it('/ Assigning too many players thrown an error.', () => {
      expect(() => {
        new Team([
          teamPlayers[0],
          teamPlayers[1],
          teamPlayers[2],
          teamPlayers[3],
          teamPlayers[4],
          teamPlayers[0],
          teamPlayers[1],
          teamPlayers[2],
        ])
      }).toThrow(new Error('Team has too many players.'));
    });

    it('/ .validatePlayerPositions() throws error if there are more than one goalkeeper.', () => {
      expect(() => {
        new Team([
          new Player(0, '', 1, Position.Goleiro, 18),
          new Player(0, '', 2, Position.Goleiro, 18),
        ])
      }).toThrow(new Error('There are too many goalkeepers in the team.'));
    });

    it('/ .validatePlayerPositions() throws error if there are more than two attackers.', () => {
      expect(() => {
        new Team([
          new Player(0, '', 1, Position.Atacante, 18),
          new Player(0, '', 2, Position.Atacante, 18),
          new Player(0, '', 3, Position.Atacante, 18),
        ])
      }).toThrow(new Error('There are too many attackers in the team.'));
    });

    it('/ .validatePlayerPositions() throws error if there are more than two defenders.', () => {
      expect(() => {
        new Team([
          new Player(0, '', 1, Position.Defensor, 18),
          new Player(0, '', 2, Position.Defensor, 18),
          new Player(0, '', 3, Position.Defensor, 18),
        ])
      }).toThrow(new Error('There are too many defenders in the team.'));
    });
  });
});
